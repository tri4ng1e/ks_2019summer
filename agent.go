package main

import (
	"fmt"
	"gitlab.com/tri4ng1e/ks_2019summer/fetch"
	"io/ioutil"
	"os"
)

func main() {
	info, err := fetch.Json()
	if err != nil {
		fmt.Println("fetch.Json() error:", err)
		return
	}

	var file string
	if len(os.Args) > 1 {
		file = os.Args[1]
	} else {
		file = "result.json"
	}

	fmt.Println(string(info))
	ioutil.WriteFile(file, info, 0644)
}
